import 'dart:io';
// import 'package:intl/intl.dart';
// import 'package:intl/date_symbol_data_local.dart'; // for other locales

// Làm 1 chiếc lịch trong đó nhập ngày tháng năm sẽ hiển thị sang ngày trước đó

// 1,3,5,7,8,10,12 31 ngay
// 2 28-29 ngay
// 4,6,9,11 30 ngay
void main(List<String> arguments) {
  while (true) {
    print('Nhập ngày: ');
    int day = int.parse(stdin.readLineSync() ?? '');
    print('Nhập tháng: ');
    int month = int.parse(stdin.readLineSync() ?? '');
    print('Nhập năm: ');
    int year = int.parse(stdin.readLineSync() ?? '');
    // DateTime previousDate =
    //     DateTime(year, month, day).subtract(const Duration(days: 1));
    // initializeDateFormatting('es'); // This will initialize Spanish locale

    // print('Ngày trước đó:  ${DateFormat.yMd('es').format(previousDate)}');
    bool dataCheck = false;
    bool leapYear = leapYearCheck(year);
    if (day <= 31 && month <= 12) {
      if (day == 1) {
        if (month == 2 ||
            month == 4 ||
            month == 6 ||
            month == 9 ||
            month == 11) {
          day = 31;
          month = month - 1;
        } else if (month == 5 ||
            month == 7 ||
            month == 8 ||
            month == 10 ||
            month == 12) {
          day = 30;
          month = month - 1;
        } else if (month == 1) {
          day = 31;
          month = 12;
          year = year - 1;
        } else if (month == 3) {
          if (leapYear) {
            day = 29;
          } else {
            day = 28;
          }
          month = month - 1;
        }
        dataCheck = true;
      } else if (day >= 29 && month == 2 && !leapYear) {
        print('Không tồn tại ngày tháng năm vừa nhập');
      } else {
        day = day - 1;
        dataCheck = true;
      }
    } else {
      print('Không tồn tại ngày tháng năm vừa nhập');
    }
    if (dataCheck) {
      print('Ngày tháng năm trước đó: $day - $month - $year');
    }
  }
}

bool leapYearCheck(int year) {
  bool leapYear = false;
  if (year % 4 == 0) {
    if (year % 100 == 0) {
      if (year % 400 == 0) {
        leapYear = true;
      } else {
        leapYear = false;
      }
    } else {
      leapYear = true;
    }
  }
  return leapYear;
}
