import 'dart:io';

//Viết 1 chương trình có sử dụng vòng lặp if else, nhập danh sách các số nguyên bao gồm tìm kiếm, tính tổng các số của list ban đầu, sửa, xóa, thêm list?
void main() {
  List listNumber = [];
  int menu = 0;
  do {
    print('1. Tạo list');
    print('2. Thêm list');
    print('3. Sửa list');
    print('4. Xóa theo vị trí trong list');
    print('5. Xóa theo số được chỉ định trong list');
    print('6. Tìm kiếm list');
    print('7. Xuất list');
    print('8. Thoát chương trình list');
    print('Nhập số: ');
    String? choose = stdin.readLineSync();
    menu = int.parse(choose ?? '0');
    switch (menu) {
      case 1:
        listNumber.clear();
        print('Tạo list thành công');
        break;
      case 2:
        List temperaryNumber = listNumber;
        listNumber = addList(temperaryNumber);
        break;
      case 3:
        List temperaryNumber = listNumber;
        listNumber = fixAtPotisitionList(temperaryNumber);
        break;
      case 4:
        List temperaryNumber = listNumber;
        listNumber = deleteByPotisitionList(temperaryNumber);
        break;
      case 5:
        List temperaryNumber = listNumber;
        print("Danh sách: $listNumber");
        listNumber = deleteByNumberList(temperaryNumber);
        break;
      case 6:
        findNumberInList(listNumber);
        break;
      case 7:
        print("Danh sách: $listNumber");
        break;
      default:
    }
  } while (menu != 8);
}

List addList(List temperaryNumber) {
  print('Nhập 1 số từ bàn phím: ');
  temperaryNumber.add(int.parse(stdin.readLineSync() ?? ''));
  return temperaryNumber;
}

void findNumberInList(List listNumber) {
  print('Nhập 1 số từ bàn phím: ');
  bool check = false;
  int postition = 0;
  int randomNumber = int.parse(stdin.readLineSync() ?? '');
  for (final number in listNumber) {
    if (number == randomNumber) {
      check = true;
      postition = listNumber.indexWhere((element) => element == number) + 1;
      break;
    }
  }
  if (check) {
    print('Tìm thấy số $randomNumber tại vị trí thứ $postition trong list');
  } else {
    print('Không tồn tại phần tử trong list');
  }
}

List deleteByPotisitionList(List temperaryNumber) {
  print('Nhập 1 số từ bàn phím: ');
  int randomNumber = int.parse(stdin.readLineSync() ?? '');
  if (randomNumber > temperaryNumber.length - 1 || randomNumber < 0) {
    print('Không có vị trí này trong list');
  } else {
    temperaryNumber.removeAt(randomNumber);
    print('Đã xóa thành công');
  }
  return temperaryNumber;
}

List deleteByNumberList(List temperaryNumber) {
  print('Nhập 1 số từ bàn phím: ');
  int listLength = temperaryNumber.length;
  int randomNumber = int.parse(stdin.readLineSync() ?? '');
  temperaryNumber.removeWhere((element) => element == randomNumber);
  if (listLength != temperaryNumber.length) {
    print('Đã xóa thành công');
  } else {
    print('Không tồn tại phần tử');
  }
  return temperaryNumber;
}

List fixAtPotisitionList(List temperaryNumber) {
  print('Nhập 1 số hiển thị vị trí từ bàn phím: ');
  int randomNumber = int.parse(stdin.readLineSync() ?? '');
  if (randomNumber > temperaryNumber.length - 1 || randomNumber < 0) {
    print('Không có vị trí này trong list');
  } else {
    print('Nhập 1 số cần chèn từ bàn phím: ');
    int numberAdded = int.parse(stdin.readLineSync() ?? '');
    temperaryNumber.insert(randomNumber, numberAdded);
    print('Đã thêm thành công');
    print('Danh sách: $temperaryNumber ');
  }
  return temperaryNumber;
}
